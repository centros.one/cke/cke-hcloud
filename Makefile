SHELL := bash
.ONESHELL:
#.SILENT:
.SHELLFLAGS := -eu -o pipefail -c
#.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
.DEFAULT_GOAL := help

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
APP_NAME ?= $(notdir $(patsubst %/,%,$(dir $(MAKEFILE_PATH))))

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

export DOCKER_BUILDKIT=1
export CI_REGISTRY_IMAGE=registry.gitlab.com/p3r.one/cke-hcloud

.PHONY: help
help:
>	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: install-deps
install-deps:
> @npm install @semantic-release/exec @semantic-release/git @semantic-release/gitlab @semantic-release/changelog -D

.PHONY: version
version:
> @npx semantic-release --generate-notes false --dry-run

.PHONY: porter-update-version
porter-update-version:
> @echo "${SEMANTIC_VERSION}" || exit 1
> sed -i -r "s|[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+|${SEMANTIC_VERSION}|g" porter.yaml

.PHONY: porter-build
porter-build:
> @${HOME}/.porter/porter build --debug --name cke-hcloud --version ${SEMANTIC_VERSION}

.PHONY: porter-publish
porter-publish:
> @${HOME}/.porter/porter publish

.PHONY: release
release:
> @npx semantic-release

