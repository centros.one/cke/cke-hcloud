# Centros Kubernetes Engine HCLOUD Provider

## Get started

### Generate credentials

```bash
porter credential generate myrelease --reference wearep3r/cke-hcloud:v0.1.0
```

### Install bundle

```bash
porter install --cred myrelease myrelease --reference wearep3r/cke-hcloud:v0.1.0
```

### Uninstall bundle

```bash
porter uninstall --cred myrelease myrelease --reference wearep3r/cke-hcloud:v0.1.0
```

### Delete the installation

```bash
porter installations delete myrelease --force
```

### Get Terraform State file

```bash
porter installations output show tfstate -i myrelease
```

## Development

### Generate credentials

```bash
porter credentials generate myrelease --file porter.yaml
```

### Install bundle

```bash
porter install --cred myrelease --file porter.yaml myrelease
```