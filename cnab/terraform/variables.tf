variable "hcloud_api_key" {}
variable "release_name" {}
variable "bundle_version" {}

variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "master_count" {}

variable "master_type" {
    default = "cx31"
}

variable "master_location" {
    default = "fsn1"
}

variable "node_count" {}

variable "node_type" {
    default = "cx31"
}

variable "node_location" {
    default = "fsn1"
}

variable "os_image" {
    default = "ubuntu-20.04"
}
