provider "hcloud" {
  token = var.hcloud_api_key
}

# Shared tags
locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "release" = var.release_name
    "provider" = "apollo"
    "bundle_version" = var.bundle_version
  }

  all_tags = merge(local.common_tags)
}

output "inventory" {
  value = templatefile("${path.module}/templates/inventory.tpl", { masters = hcloud_server.master, nodes = hcloud_server.node })
}

resource "hcloud_ssh_key" "ssh_key" {
  name       = join("-",[var.release_name, "ssh"])
  public_key = file(var.ssh_public_key)
}

resource "hcloud_server" "master" {
  count = var.master_count
  name = join("-",[var.release_name,"master",count.index])
  image = var.os_image
  server_type = var.master_type
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = local.all_tags
  location = var.master_location
}

resource "hcloud_server" "node" {
  count = var.node_count
  name = join("-",[var.release_name,"node",count.index])
  image = var.os_image
  server_type = var.node_type
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = local.all_tags
  location = var.node_location
}
